//
//  AppDelegate.h
//  TestFW
//
//  Created by moofwd on 9/10/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <McxIoSDK/MeDeeMFramework.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
